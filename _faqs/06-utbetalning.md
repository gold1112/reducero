---
question: När betalas lånet ut?
---
Beroende på vilken långivare som du väljer så kan det ta olika lång tid för dig att erhålla dina pengar.

**Ett skuldebrev skickas till dig med posten** vilket du skriver under och retunerar. När långivaren fått ett signerat skuldebrev i retur tar det ca 2-5 arbetsdagar innan pengarna finns på ditt bankkonto.

**Du öppnar en länk till ett digitalt skuldebrev** som du skriver under med BankID. Pengarna kan sättas in på ditt bankkonto eller skickas till dig i form av en utbetalningsavi som du sedan kan gå med till en bank som hjälper dig att lösa in den. Du får vanligtvispengarna utbetalda till ditt bankkonto redan samma dag om du signerar skuldebrevet med BankID.

**Du går direkt till långivarens kontor** under kontorstid och skriver under ett skuldebrev och får pengarna utbetalad eller överförda till ditt bankkonto.

**Viktigt.** Avvikelser kan förekomma då Reducero inte kan påverka hur lång tid varje långivare behöver för att behandla ditt ärende.