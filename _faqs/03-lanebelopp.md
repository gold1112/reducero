---
question: Hur mycket kan jag låna? 
---

Med Reducero kan du låna från 20 000 kronor upp till 500 000 kronor.

Bankerna prövar din ansökan för att ta reda på din ekonomiska situation och för att bedöma din framtida förmåga att betala tillbaka lånet. Utifrån detta bestämmer vardera bank och långivare hur mycket du kan låna.

**Tips.** En bra riktlinje är att inte ansöka om ett lån som överstiger mer än 90% av din årsinkomst.
