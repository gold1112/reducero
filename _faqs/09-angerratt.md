---
question: Kan jag lösa lånet i förtid?
---

Du kan när som helst lösa lånet i förtid eller betala in extra pengar utan att det kostar någonting.

Kontakta banken eller låneinstitutet så hjälper de dig.