---
question: Vilka är grundkraven? 
---

* Vara minst 18 år
* Vara folkbokförd och skriven i Sverige minst ett år
* Har en inkomst från arbete eller pension
* Inneha en årsinkomst på minst 110 000 kr
* Vara skuldfri från kronofogden i minst 6 månader
