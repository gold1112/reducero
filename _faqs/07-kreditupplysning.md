---
question: Får jag bara en (1) kreditupplysning?
---
Endast en kreditupplysning hos Upplysningcentralen (UC) kommer att registreras på dig, men du kan få flera omfrågandekopior från de banker som har tittat på din kreditupplysning. I enstaka fall kan vissa banker ta in ytterligare information som tillägg till informationen från UC för att förbättra kvaliteten i kreditbeslutet.