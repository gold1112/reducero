---
question: Hur lång är återbetalningstiden?
---
Du väljer återbetalningstid själv, från 1 år upp till 15 år. För mindre lån kan det vara lönsammare att ha en kortare återbetalningstid. Totala beloppet att återbetala stiger vid längre återbetalningstid.