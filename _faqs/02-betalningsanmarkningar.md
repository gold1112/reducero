---
question: Kan jag låna med betalningsanmärkning?
---

Nej du kan inte låna om du har betalningsanmärkningar eller skuld hos kronofogden.
