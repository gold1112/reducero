---
layout: cookies
title: Personuppgiftspolicy - Reducero
description: Här kan du läsa mer om hur Reducero bevarar dina personuppgifter, ett nödvändigt ting för att du ska kunna använda Reducero.
keywords: cookies reducero, cookies, cookieinformation

sitemap:
   priority: 0.9
   
permalink: /personuppgiftspolicy/
---

# Personuppgiftspolicy