---
layout: seo
title: Seniorlån | Reducero
description: Seniorlån när du behöver låna pengar med bostad som säkerhet.
keywords: seniorlån
headline: Om seniorlån
enable-CTA: true

sitemap:
   priority: 0.9
   
---

### Seniorlån - för vem

Ett seniorlån är ett lån för dig som är 60 år eller äldre och har en bostad. Det kan vara en bostadsrätt eller ett hus likväl som ett sommarhus, men du behöver ha en fastighet att belåna. Du kan då med seniolånet frigöra pengar ur ditt boende utan att sälja det men ändå använda de pengar du har i bostaden. Det krävs dock att din bostadsrätt är värd minst 500 000 eller att ditt hus eller fritidshus har ett värde på minst 900 000 för att du ska ha möjlighet att ta ett seniorlån. 

### Hur mycket kan du låna 

Värdeutvecklingen på bostäder har idag utvecklats enormt och du kan sitta på lägenhet med stort kapital bundet och vilja ha möjlighet att utnyttja detta. Med ett seniorlån kan du låna upp till 45% av värdet på din bostad. Detta lån är ett perfekt sätt att är dryga ut pensionen så du kan ha råd med din drömresa eller en nödvändig renovering. Det är också ett tryggt sätt att låna då du aldrig behöver oroa dig om att bli skyldig mer än du lånat, då du aldrig kan låna mer än vad din bostad är värd. Du frisätter helt enkelt det kapital som finns i din bostad utan att sälja huset eller lägenheten.

### Seniorlån istället för bolån

Ett seniorlån är även ett bra alternativ för dig som redan har ett bolån. Ditt vanliga bolån behöver du amortera och betala ränta på medan du inte behöver avbetala alls på ditt seniorlån. Detta kan ge dig mer ekonomisk frihet och möjlighet att sätta guldkant på din vardag som senior. Det går även att betala eventuella andra dyra lån med hjälp av ett seniorlån och på så vis sänka din månadskostad och få pengar över till annat. 

### Reducero - låna utan säkerhet

Om du inte vill belåna din bostad eller kanske inte har en bostadsrätt eller hus så finns det en annan möjlighet, nämligen ett lån utan säkerhet där du kan låna upp till hela 400 000 kronor. När du väl bestämt dig för att ta ett lån utan säkerhet gäller det också att välja rätt långivare. Det är en djungel att hitta rätt och självklart oerhört viktigt att få de allra bästa förutsättningarna för ditt lån. Det är också tryggt att använda dig av Reducero då du enkelt kontaktar kundtjänst vid funderingar kring ditt lån, vare sig det är stora eller små frågor du funderar över.

{% include banner-cta.html %}