---
layout: seo
title: Topplån & bottenlån - Reducero
description: Vi berättar allt om topplån, det ger dig bäst förutsättningar att låna pengar till kontantinsatsen med bra villkor och låg ränta.
keywords: topplån, topplån och bottenlån, topplån ränta
headline: Bra att veta om topplån
enable-CTA: true

sitemap:
   priority: 0.9
   
---

### Vad är ett topplån?

Topplån är det lån som många väljer att ta för att täcka den delen av bostadslånet som inte kan räknas som bottenlån, numer 15 procent. Kort sagt utgör ett topplån den del av lånet på bostaden som inte täcks av pantbrevet. Lånet har oftast högre ränta och kortare betalningstid då det innebär en högre risk för lånegivare, eftersom det vanligtvis är ett lån utan säkerhet.

#### Skillnaderna mellan topplån, bottenlån och blancolån

Med bostaden som pant får du låna upp till 85 procent av köpeskillingen, detta lån kallas för bottenlån. Den överskjutande delen av köpeskillingen, 15 procent, ska betalas med befintliga medel eller via ett topplån. Långivarna vill försäkra sig om att få tillbaka sina utlånade pengar även om bostadsmarknaden skulle gå ner och värdet på din bostad minska. Säkerhetsmarginalen är 15 procent, så om priserna skulle falla så mycket är det alltså enbart din egen insats som försvinner. Först när marknadsvärdet på bostäder gått ned med 25 % riskerar långivaren att förlora pengar genom bottenlånet. 
Om du saknar hela eller delar av de 15 procent som krävs som kontantinsats, kan du ta ett lån utan säkerhet dvs. ett blancolån. Ett topplån är ett blancolån med syftet att låna till en bostads kontantinsats.

#### Fördelarna med ett topplån
Utifall att fastigheten skulle stiga i värde finns det en möjlighet att lägga om lånen, så att en större del av beloppet från topplånet kan läggas över på bottenlånet. Det kan vara värt att ta ett blancolån för att genomföra värdeförbättrande renoveringar, och sedan göra en omvärdering av bostaden för att kunna baka in topplånet i bottenlånet.

#### Riskerna med ett topplån

Långivaren har ingen säkerhet för den överskjutande delen av ditt bostadslån, topplånet, och därför är räntan normalt sett högre. En tråkig konsekvens av en bostadsmarknad som går ner kan vara att din bostad minskar i värde och därmed har du ett högre lån än värdet på din bostad. En annan risk kan vara att du hastigt tvingas att flytta och måste sälja din bostad snabbt, den köpeskilling som du erhåller kanske då inte motsvarar vad du själv betalade och då har du hela eller delar av ett topplån kvar att betala en tid framöver.

### Att låna till topplånet

Den senaste tidens ökningar av bostadspriser i kombination med ökade krav på kontantinsats gör att många väljer att låna till topplånet. 
Tidigare brukade många bostadsköpare vara tvungna att gå runt till olika långivare och be om att få låna pengar till topplån. Ditt topplån kan vara en stor del av din utgift varje månad, så var noga med att jämföra ordentligt innan du bestämmer dig. Med några få klick hjälper Reducero dig med detta och du kan i lugn och ro, i ditt hem, bestämma dig för vem som ska få låna ut pengar till dig.

{% include banner-cta.html %}
