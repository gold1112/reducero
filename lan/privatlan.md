---
layout: seo
title: Jämför privatlån med Redcuero
description: Jämför privatlån, det ger dig bäst förutsättningar att tecka privatlån med bra villkor och låg ränta.
keywords: privat lån, privatlån, privatlån ränta
headline: Fakta om Privatlån
enable-CTA: true

sitemap:
   priority: 0.9
   
---

### Generellt om privatlån

Privatlån även kallat blancolån är ett av de vanligaste lånen nästa efter bolån. Ett privatlån är ett lån utan säkerhet, till skillnad mot bolånet där bostaden är bankens säkerheten för lånet. Med ett privatlån är det istället den personliga betalningsförmågan som utgör en form av säkerhet, man kan alltså säga att ens framtida inkomst fungerar som en sorts säkerhet för lånet.

### Vad kan jag använda pengarna till
Då ett privatlån är ett lån utan säkerhet avsett för konsumtion kan man använda pengarna till precis vad man vill. Långivarna behöver endast veta syftet med lånet men frågar inte exakt vad du skall köpa och pengarna kan inte tas ifrån dig, i alla fall inte av långivaren men däremot av Kronofogden.

Det är vanligt att man använder ett privatlån för att köpa något som har en högre kostnad, så som begagnade bilar, motorcyklar och andra fordon. Det är också vanligt att låna till renovering och kontantinsats vid bostads- eller bilköp.

### Hur mycket får jag låna?

Hur mycket du får låna beror på din nuvarande ekonomi och din framtida återbetalningsförmåga. 

Du kan ansöka om att låna upp till 500 000 kronor med Reducero. Utifrån de uppgifter du lämnar och den kreditupplysning Reducero beställer bedömer alla långivare din ekonomi och framtida betalningsförmåga och det är just deras bedömning som ligger till grund för hur mycket de erbjuder dig att låna och med vilken ränta.

Tänk på att inte låna mer pengar är du är i behov av. Det är viktigt att du också kan betala ränta och amortera även om du skulle bli sjuk eller arbetslös. En bra riktlinje är att inte ansöka om ett privatlån som överstiger 90% av din årsinkomst.

### Vilken ränta får jag på privatlånet

Räntan sätts inte av Reducero utan av de långivare som är anslutna till Reducero. Räntan sätts individuellt för varje låntagare och varierar beroende på din nuvarande ekonomi,  framtida betalningsförmåga och långivarens riskbedömning. 

Det är viktigt att skilja på räntan och den effektiva räntan. Reducero räknar alltid fram den effektiva räntan som inkluderar alla eventuella avgifter kopplade till lånet. Det är först när du jämför den effektiva räntan mellan långivarna som du får en rättvis jämförelse av den faktiska kostnaden.

Den effektiva räntan inkluderar samtliga kostnader så som ränta, avi- och faktureringsavgifter och eventuell uppläggningsavgift. Hur den effektiva räntan beräknas och presenteras är reglerat i Konsumentkreditlagen.

### Återbetalning

Du betalar tillbaka samma summa varje månad, summan inkluderar ränta och amortering. Utöver räntan och amortering tillkommer eventuell aviavgift och upplägningsavgift. Räntan och avgifterna är rena kostnader och skall täcka långivarens kostnader för upplåning och administration.

Du kan när som helst betala av hela eller delar av ditt lån, utan extra kostnader och oavsett vilken långivare du tecknat lånet hos. Kontakta din långivare för att göra upp en alternativ återbetalningsplan.

### Grundkrav för privatlån

Det finns ett antal grundkrav du behöver uppfylla innan du ansöker om ett privatlån, du behöver bland annat ha en fast inkomst från arbete eller persion och att du är folkbokförd i Sverige. Du får inte heller ha några skulder hos kronofogden.

[Läs mer om vilka grundkrav för privatlån här.](#faq)
{% include banner-cta.html %}
