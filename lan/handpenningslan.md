---
layout: seo
title: Handpenningslån - Låna pengar till handpenningen | Reducero
description: Handpenningslån när du behöver låna pengar till kontantinsatsen - Jämför lån med Reducero innan du lånbar till handpenningen
keywords: handpenningslån, låna till handpenning
headline: Bra att veta om Handpenningslån
enable-CTA: true

sitemap:
   priority: 0.9
   
---

### Vad är en handpenning?

Allt som oftast måste du betala handpenning några dagar efter att du t.ex. tecknat ett kontrakt för ett nytt boende, handpenningen brukar motsvara tio procent av köpeskillingen och pengarna betalas oftast till mäklaren som håller dom till dess att affären är slutförd.

Om du saknar kontanter vid inköpstillfället kan du istället ansöka om ett lån som du sedan använder för att betala handpenningen.

### Varför väljer andra handpenningslån?

Handpenningslån finns till för de som med kort varsel ska betala en handpenning men inte har kontanter tillgängliga vid tillfället, de kan vara uppbunda på sparkonto, aktier, föremål eller andra former som gör att det tar längre tid för dig att frigöra kapitalet.

### Vad är det för skillnad på handpenning och kontantinsats

Handpenning är en form av deposition som vid bostadsköp motsvarar 10% av köpeskillingen som du betalar när du tecknar köpekontraktet, medan kontantinsatsen motsvarar 15% av bostadens pris som betalas först vid själva tillträdet av bostaden.

### Låna till handpenning

När du ska låna pengar för ditt bostadsköp så får du via ett vanligt hypotekslån endast låna 85% av bostadskostanden. Du har då alltså ett resterande belopp på 15% som du behöver låna på annat sätt till. För att finansiera detta belopp behöver du ett så kallat topplån, men det är inte alltid möjligt att få just ett topplån. Det du då istället kan göra är att ta ett blancolån när du ska låna till handpenning, det vill säga ett lån utan säkerhet. Ditt hypotekslån har den bostad du köper som säkerhet men med ett lån utan säkerhet kan du låna upp till 400 000 kronor utan att ha en säkerhet bunden till lånet. På så vis kan du låna till handpenning och ha möjlighet att köpa din drömbostad.

### Hur lånar jag till handpenningen?

Det går bra att finansiera handpenningen med ett lån utan säkerhet även om grundtanken är att det ska vara ditt sparade kapital. 

Med Reducero jämför du ett flertal långivares räntor innan du lånar pengar, du kan ansöka om att låna upp 500 000 kr och en återbetalningstid på upp till 12 år.

{% include banner-cta.html %}