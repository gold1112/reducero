---
layout: seo
title: Banklån - Jämför långivarnas ränta och spara pengar | Reducero
description: Du kan ansöka om ett banklån direkt, du kan låna upp till 500 000 kronor och använda pengarna till vad du vill.
keywords: banklån, banklån ränta
headline: Bra att veta om Banklån
enable-CTA: true

sitemap:
   priority: 0.9
   
---

### Generellt om blancolån
  Näst efter bolånet är blancolån ett av de mest vanliga lånen. Det är ett [lån utan säkerhet](/lan) och kallas också vanligen för [privatlån](/lan/privatlan). Till skillnad mot bolånet krävs alltså ingen säkerhet för lånet utan det är istället din framtida återbetalningsförmåga som långivaren ser som en form av säkerhet, man kan alltså säga att ens framtida inkomster fungerar som en sorts säkerhet för blancolån.

### Vad får jag använda pengarna till?

Ett blancolån är avsett för konsumtion och du kan använda pengarna till precis det du önskar. Långivaren vill endast veta syftet med lånet men frågar inte mer specifikt efter vad pengarna ska användas till. De kan heller inte ta pengarna ifrån dig, det kan däremot Kronofogden göra.

Blancolån används oftast för att köpa något som har en lite högre kostnad, t.ex. en bil, motorcykel eller ett fritidshus. Det är också vanligt att låna till att renovera badrum, kök eller till kontantinstasen inför bil och bostadsköp.

### Hur stort lån kan jag ansöka om?

Med Reducero kan du ansöka om att låna mellan 5 000 och 500 000 kronor. Baserat på de uppgifter du lämnat och den kreditupplysning Reducero beställer avgör alla långivare hur mycket pengar du får låna. 

Det är alltså respektive långivare som är ansluten till Reducero som bestämmer hur mycket du får låna baserat på din nuvarande ekonomi och framtida betalningsförmåga.

Ett riktmärke är att inte låna mer än 90% av din årsinkomst och att räkna med att ha råd att betala räntan även om du blir sjuk eller arbetslös.

### Amortering och återbetalningar

Du betalar alltid tillbaka samma belopp varje månad och beloppet inkluderar både ränta och amortering. Utöver räntan och amortering kan det också tillkomma aviavgift och en upplägningsavgift. Allt utom amorteringen är rena kostnader som ska täcka långivarens alla kostnader för bland annat upplåning och administration.

Ett blancolån kan betalas tillbakas helt eller delvis innan avtalad återbetalningstid utan extra kostnad. Om du vill göra upp en alternativ återbetalningsplan ska du kontakta din långivare direkt.

### Vem får ansöka?

Du behöver uppfylla ett par grundkrav innan du ansöker om ett blancolån, du behöver bland annat vara svensk medborgare och bosatt i sverige, en fast inkomst från pension eller arbete och du får inte ha några skulder hos Kronofogden, betalningsanmärkningar kan dock accepteras.

[Läs mer om vilka grundkrav för blancolån här.](#faq)

{% include banner-cta.html %}

