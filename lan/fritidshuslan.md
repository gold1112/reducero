---
layout: seo
title: Låna till fritidshus utan kontantinsats | Reducero
description: Jämför långivarna räntor innan du köper fritidshus, det ger dig bra chanser att låna till fritidshuset med låg ränta och bra villkor.
keywords: låna till fritidshus, låna till fritidshus utan kontantinsats
headline: Bra att veta när du lånar till fritidshus
enable-CTA: true

sitemap:
   priority: 0.9
   
---

### Två typer av fritidshus
När man talar om fritidshus så brukar man dela upp ägandeformerna i två; friköpt och arrenderad tomt. När du köper en sommarstuga på en friköpt tomt så äger du både tomten och byggnaderna. Om objektet istället ligger på en arrenderad tomt så hyr du marken av markägaren, oftast en längre tid, det kallas även att huset står på en ofri grund. Vid arrende äger du alltså endast byggnader och innehållet i dem.

### Att låna till fritidshus
Det är lite olika låneförutsättningar beroende på om det rör sig en arrendetomt eller en friköpt tomt. När du lånar till en friköpt sommarstuga kan du ta ett bolån med fastigheten som säkerhet på upptill 85 procent av värdet. 15 procents kontantinsats får du själv stå för. Med ett privatlån, kan du även låna till fritidshus på hela summan eller låna endast till kontantinsatsen.

Om du hittat ett arrendehus som du vill köpa, säger traditionella långivarna oftast nej till ett sådant fritidshuslån. Fördelen med ett fritidshus på arrendetomt är att det oftast är mycket billigare i inköp, läget kan vara väldigt bra (det är exempelvis inte ovanligt med sjötomt). Om du vill låna till fritidshus på arrendetomt är ett blancolån (även kallat privatlån) en bra och kanske den enda lösningen. 

### Går det att låna till fritidshus utan kontantinsats?
Ja det går att [låna pengar](/lana-pengar) till kontantinsatsen, det går även att låna till fritidshuset som sådant. Reducero förmedlar endast privatlån utan säkerhet på fritidshuset. Oavsett om du ska köpa en ny sommarstuga eller renovera det hus du redan har så kan vi hjälpa dig att hitta en fördelaktig finansieringslösning. 

### Jag vill låna till fritidshus, vilken blir räntan?
Räntan beror på hur mycket du vill låna till ditt fritidshus och vilken återbetalningsförmåga du bedöms ha. Eftersom ingen säkerhet tas baseras räntan på risken för lånet och därmed spelar din inkomst stor roll. Även din ekonomiska situation i övrigt tas i beaktande. Ansökan är enkel, du får svar snabbt och du kan låna upp till 400 000 kr!

### Kan jag ha en medlåntagare?
Ja, förutsättningarna är betydligt större att få låna till fritidshus om ni är två som står på lånet.

### Att tänka på innan du köper fritidshus

Innan drömmen om ett fritidshus kan bli verklighet bör ni först och främst se över om ni har råd. Även om ni lånar till fritidshuset och på så sätt sprider ut betalningen, kan oförutsedda avgifter ställa till problem. Låt en besiktningsman göra en ordentlig koll av huset och fastigheten. Ta reda på om det finns något dolt servitut och vad det i så fall innebär. Undersök om möjligheterna att renovera och bygga ut begränsas av någon form av lag eller avtal. Slutligen, glöm inte att jämföra bästa finansieringslösningen för dig och ditt fritidshus. Reducero hjälper dig som vill låna till fritidshus att spara upp till tusentals kronor per månad. Genom att tvinga långivare att konkurrera om dig som kund kan du räkna med att få bästa möjliga ränta.

{% include banner-cta.html %}