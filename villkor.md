---
layout: cookies
title: Cookies - Reducero
description: Här kan du läsa mer om hur Reducero använder Cookies, ett nödvändigt ting för att du ska kunna använda Reducero.
keywords: cookies reducero, cookies, cookieinformation

sitemap:
   priority: 0.9
   
permalink: /villkor/
---

# Reduceros användaravtal

Reducero är en kreditförmedlare som tillhandahåller en tjänst där du kan jämföra lån och villkor hos de långivare som Reducero samarbetar med (“Tjänsten”). Genom en ansökan kan du få erbjudanden om lån från flera långivare. På så sätt kan du jämföra villkor från flera aktörer och välja det alternativ som passar dig bäst. Du kan använda dig av Tjänsten när du vill samla existerande lån och t.ex. kreditkortsskulder eller när du önskar ta ett nytt lån. Fördelen med att använda Reduceros Tjänst istället för att jämföra direkt hos långivarna är att det då endast tas en kreditupplysning istället för flera och din kreditvärdighet bevaras. Tjänsten tillhandahålls av Reducero AB (org.nr 556957-7728) med tillstånd att driva konsumentkreditinstitut av Finansinspektionen.

## Kreditupplysning
Beställning av kreditupplysning görs hos UC AB. En beställning innebär att UC registrerar en så kallad omfrågning på dig. I Reduceros UC-lösning registreras en förfrågan när du skickar in din ansökan. Därefter kan alla långivare som samarbetar med Reducero under begränsad tid beställa en kreditupplysning på dig utan att ytterligare frågor registreras.

## Personuppgiftslagen
Personuppgiftslagen (PUL 1998:204) trädde i kraft i oktober 1998 och gäller fullt ut från och med den 1 oktober 2001. Lagen ger dig som kund rätt till information om behandling av personuppgifter.

## Personuppgiftsansvarig
Reducero AB med organisationsnummer 556957-7728 är personuppgiftsansvarig för de uppgifter som du lämnar.

## Personuppgifter och användning
Vi lagrar de personuppgifter du lämnar i samband med att du registrera dig för en av våra tjänster. Vi använder personuppgifterna för att kunna fullgöra våra åtaganden gentemot dig avseende den tjänst du beställt och för att upprätthålla en god kund- och registervård. Uppgifterna används vidare för att hantera dina kundärenden samt för information och marknadsföring via post, telefon eller elektronisk kommunikation så som sms, e-post eller brev. Uppgifterna kan också komma att användas i utvecklingen av nya produkter och tjänster och för att ge dig anpassat innehåll i våra tjänster, t.ex. erbjudanden och annonser som är mer relevanta för dig. Uppgifterna kan komma att lämnas ut till Reducero AB:s samarbetspartners i de syften som framgår ovan. Du kan när som helst rätta eller uppdatera felaktiga person-, adress- och telefonuppgifter genom att kontakta oss på kundservice@reducero.se. Du kan även kontakta oss om du inte vill att vi använder dina personuppgifter i samband med marknadsföring. Uppgifterna kommer i dessa fall att spärras för sådan användning. Du har också möjlighet att få information om vilken information som finns sparad om dig. Detta gör du genom att kontakta Reducero AB:s personuppgiftsombud på kundservice@reducero.se.

## Ändringar
Vi kan komma att göra ändringar i vår personuppgiftspolicy. Vid uppdateringar som är av avgörande betydelse för vår behandling av personuppgifter eller uppdateringar som inte är av avgörande betydelse för behandlingen men som kan vara av avgörande betydelse för dig, kommer du att få information på Reduceros hemsida och/eller via e-post (om vi har sådan uppgift) i god tid innan uppdateringarna börjar gälla.

Version 201801