---
layout: posts
title: Det du måste veta innan du säljer din bil - Reducero
description: Detta måste du veta innan du säljer din bil
keywords: sälja bil, billån, köpa bil
anchor: Det du måste veta innan du säljer din bil
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/salja-bilen

sitemap:
   priority: 0.8
   
tags:
  - Bil
  - Billån
  - Blancolån

headline: Det du måste veta innan du säljer din bil
subline: ...så blir försäljningen till en dans
background: /img/2016-07-14-salja-bilen.jpg
---

Har gamla Bettan börjat krångla och orsakar dyra reparationskostnader? Har familjen växt ur den lilla bilen som var perfekt tidigare? Har grannens nyköp fått dig att sukta efter en helt ny bil? Oavsett vad anledningen är till att du vill sälja bilen, finns det en hel del saker att tänka på. Läs på lite nedan innan du gör dig av med din gamla bil.

### Spara pengar genom att byta bil
Kan du spara pengar på att byta bil? Svaret är ja. En gammal bil kräver allt fler reparationer, i extrema fall dyrare reparationer än själva värdet på bilen. Men det är inte bara reparationerna som blir dyrare med bilens ålder, nyare bilar är generellt sett mer bränslesnåla och ger mer körglädje. Köp en bil du har råd med, som sparar dig pengar. Att köpa en lyxbil innebär inte bara en dyr initial kostnad. Tänk även på att bilens försäkringspremie generellt blir högre för en dyrare bil. Jämför försäkringspremier för olika typer av bilar. Se upp med att ovanliga bilmodeller ofta medför dyrare reservdelar. Var noggrann med att jämföra bränsleförbrukning mellan olika bilmodeller. Med dagens bensinpriser kan du spendera en förmögenhet på färdmedel för en gammal bränsleslukande bil. Om du köper en bil med lägre bränsleförbrukning kan du lägga de pengar du sparar på annat roligt istället. 

Är det bäst att köpa en helt ny bil? Nej. Värdet på en ny bil minskar i snitt med 25-40 procent under de första två åren. Att låta någon annan stå för den första värdeminskningen är en mycket bra idé. Om du köper en två år gammal bil är bilen fortfarande i bra skick, och du kan få nybilskänslan för nästan halva priset. Tidigare var garantin en bra anledning till att köpa en helt ny bil, men idag har bilarna oftast längre garantier som kan förlängas till en rimlig penning. 

### Hur du säljer bäst
Att lämna in din bil som ett inbyte hos en bilhandlare kan låta som en bra idé, men du förlorar tyvärr mycket pengar på det. En bilhandlare kalkylerar med minst 20 procent marginal, pengar som skulle kunna hamna i din ficka istället. Sälj din bil privat – och du sparar tusenlappar.

Förbered bilförsäljningen noggrant. Alla småfel så som; trasiga lampor, fläckar på inredningen och repor i lacken bör justeras innan du lägger ut bilen till försäljning. Lämna in bilen på service om så behövs och se över serviceboken. Tvätta bilen utvändigt samt dammsug och städa invändigt. Fotografera bilen i dagsljus, i flera olika vinklar, med en fin bakgrund som inte tar allt för mycket uppmärksamhet. Största onlinesiten för försäljning av begagnade bilar är blocket.se. Vi rekommenderar att du lägger in annonsen med en säljande annonsrubrik och text, när trafiken är som störst, exempelvis måndag morgon. Sätt inte ett för högt pris, kolla runt på nätet vad din bil går för och undvik att överdriva – det kan skrämma bort seriösa spekulanter. En liten prutmån är rimligt, om köparen inte får förhandla om priset kan de välja att dra sig ur.

[<img src="/img/2016-07-14-salja-bilen-banner.png" width="100%" alt="billån, bil lån">](/lan/billan)

När det är dags för affär är det viktigt att ni skriver ett giltigt köpeavtal, du kan hitta en mall på nätet för utskrift. Lämna inte ifrån dig bilen innan du har fått full betalning. Ofta sker slutbetalning genom postväxel, eller betalning via internetbanken direkt. När affären är klar ska du säga upp din försäkring (sker ibland automatiskt när försäkringsbolaget får besked om ägarbytet genom bilregistret), [kolla skatten och genomföra ett ägarbyte](http://www.transportstyrelsen.se/agarbyte).

### Köpa ny bil med hjälp av billån
För många bilägare är bilen en nödvändighet för att få vardagen att gå ihop. Vi är ofta beroende av vår bil för att ta oss till jobbet och för att köra barnen till fritidsaktiviteter. Bilen är även en stor frihet och ett fritidsnöje för bilintresserade. Eftersom att bilen är så pass viktig för oss, bör du välja en bil som är pålitlig, bekväm och som passar dina behov. Få människor betalar bilen kontant. De flesta väljer istället att dela upp betalningen i form av ett [billån](/lan/billan). Om du inte har någon tillgång att belåna är det vanligaste finansieringsalternativet ett blancolån. Ett sådant billån tas utan säkerhet, med din inkomst som försäkran. Med ett [blancolån](/lan/blancolan) behöver du inte betala handpenning, om du inte vill. Du kan låna upp till hela summan för att köpa en ny bil. 
