﻿---
layout: posts
title: 5 saker du bara måste veta när du skall köpa din första bostadsrätt! - Reducero
description: Råden som gör köpet till en dans - 5 saker du bara måste veta när du skall köpa din första bostadsrätt.
keywords: bostadsrätt, köpa bostadsrätt
anchor: 5 saker du bara måste veta när du skall köpa din första bostadsrätt!
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/bostadskop
sitemap:
   priority: 0.8
   
tags:
  - bostad
  - köpa bostad
  - topplån
  - bostadslån
  - bostadsrätt
headline: 5 saker du bara måste veta när du skall köpa din första bostadsrätt!
subline: Råden som gör köpet till en dans
background: /img/2016-05-11-bostadskop.jpg
---


### Finansiering och lånelöfte.

Det första steget mot att få din drömlägenhet är att ordna med finansiering och lånelöfte. Du måste räkna på hur hög boendekostnad du har råd med, hur dyr lägenhet du kan köpa och ta reda på hur mycket du kan få låna av din bank.

Som en tumregel låter banken dig låna högst 85 % av lägenhetens värde. Det innebär att om en lägenheten säljs för 1 000 000 kronor så låter banken dig låna högst 850 000 kronor.

Om lägenheten säljs för 1 miljon kronor behöver du alltså spara ihop eller på annat sätt finansiera 150 000 kronor själv. Och om lägenheten är dyrare än så då behöver du spara ihop mer.

Du räknar lätt ut hur mycket du själv behöver spara ihop till handpenning eller finansiera på annat sätt genom att multiplicera 0,15 gånger med det pris du tror blir lägenhetens slutpris efter att budgivningen är klar.

Hur mycket en lägenhet budas upp varierar mycket beroende på var i landet lägenheten finns, hur många andra lägenheter som säljs samtidigt och hur många som är intresserade av just den lägenheten du vill ha. I storstadsregionerna är det dock inte ovanligt att slutpriserna är uppemot en femtedel högre än utgångspriset eller som det så ofta heter, det accepterade priset. Ofta är det tillfälligheter som styr och hur du riktigt tur kan du till och med få din drömlägenhet under utgångspris.

En annan tumregel som är viktig att känna till är att banken vanligtvis inte lånar ut mer än 5-6 gånger din årsinkomst. För en dyrare lägenhet kan det alltså många gånger vara detta som grusar planerna. Om du har möjligheten kan en medlåntagare eller borgensman göra att kalkylen går ihop och är ni två som köper lägenheten läggs era respektive inkomser ihop.

### Kolla upp försäljningar och slutpriser i området

Nästa viktiga fråga är att bestämma i vilka områden du vill bo och hur priserna i det områdena ser ut?

Samma typ av lägenhet i två olika stadsdelar kan ha helt olika slutpriser. Vissa platser är helt enkelt mer populära att bo på och därmed dyrare men samma område kanske förhoppningsvis inte lockar dig lika mycket. Det första du bör göra är därför att rikta in dig på ett område där du vill bo och sedan börja bilda dig en uppfattning om priserna i det området.

Ett bra sätt att få reda på slutpriser är att använda Hemnets och Boolis tjänster. Du kan söka på områden eller en gatuadress och därefter se slutpriserna för de lägenheter som sålts i de områdena. I områden där det säljs många lägenheter är det lättare att bilda sig en uppfattning om priset än i områden där det säljs få.

Om det säljs få lägenheter i ett område eller om det var länge sedan någon lägenhet såldes i det området kan det vara svårt att veta vad marknadspriset är idag. Priserna på lägenheter varierar ju som bekant med tiden. Om du inte kan bilda dig någon uppfattning med Hemnet och [Boolis](https://www.booli.se/slutpriser) verktyg kan du även prata med en mäklare eller försöka hitta priser för lägenheter som är jämförbara och ligger i jämförbara eller närliggande områden.

### Gå på många visningar

Många underskattar värdet av att gå på visningar. Börja med att gå på visningar tidigt, även om du inte ska köpa en lägenhet just precis nu. Du får då en känsla för vad du gillar respektive inte gillar med olika lägen. Du lär dig hur en visning går till, vilka frågor som kan vara bra att ställa till mäklaren och du blir helt enkelt bekväm i situationen. Det går ofta fort mellan visning och försäljning, så att ha vanan inne skadar verkligen inte.

house-appartement-corner-medium

Det är också ofta stor skillnad på hur lägenheten ser ut på bilder och hur lägenheten uppfattas i verkligheten. En del mäklare använder sig exempelvis av vidvinkelobjektiv vilket gör att rummen ser betydligt större ut på bild än i verkligheten.

Genom att gå på visningar får du också en känsla för områden. Ju fler lägenheter du har tittat på och kan jämföra, desto säkrare blir du på vad du egentligen letar efter, vilken typ av lägenhet som passar dig, och vad olika bostadsrätter i olika området kostar. När du sedan hittar din drömlägenhet så står inte ovana eller rädsla ivägen för att lägenheten skall bli din!

### Gör en budget
Det är ofta en bra idé att göra upp en budget. Utöver kostnaden för ränta och [amortering](https://sv.wikipedia.org/wiki/amortering) tillkommer kostnader för årsavgiften, försäkringar, el, vatten, bredband och telefoni, m.m. Vissa av dessa kostnader kan ingå i den årsavgift som du betalar en gång i månaden till föreningen (”hyran”).

Årsavgiften skiljer sig också mycket mellan olika föreningar där äldre, och rikare föreningar, brukar ha en lägre avgift.

Det är viktigt att veta att du har råd att bo kvar även om dina kostnader skulle öka eller om du tillfälligt blir av med jobbet. Om årsavgiften inte betalas kan lägenheten i värsta fall annars tvångsförsäljas.

De flesta långivare kräver numera även att du skall amoterta på lånet och räknar med att du ska klara av en högre räntenivå än dagens. Banken försöker på olika sätt räkna ut ifall du i framtiden klarar av alla dina kostnader för boendet och ändå har kvar pengar till en skälig levnadskostnad.

Hur stor del av din inkomst du vill lägga på boendet är en annan viktig fråga och handlar om prioriteringar. En del föredrar att bo enklare och har mer pengar över till nöjen och annat medan andra prioriterar boendet högre.

Försök också räkna ut hur stor handpenning som du tror att du kommer att behöva. Handpenningen motsvarar de 15 % som banken inte får låna ut till dig för att finansiera lägenhetsköpet. Lägg sedan upp en plan för hur du skall kunna spara ihop till beloppet eller undersök möjligheterna att teckna ett s.k. blanco- eller topplån. Du kan läsa mer om [blancolån](/lan/blancolan) och [topplån](/lan/topplan) på Wikipedia.

### Vänta inte för länge

Allt fler lägenheter säljs innan visning. Om du har hittat din drömlägenhet kanske du skall överväga att lämna ett bud innan visning. Ibland accepterar säljaren detta för att få ett snabbt avslut.

Det kan också vara en bra ide att anmäla ditt intresse till de större mäklarfirmorna. Långt ifrån alla lägenheter kommer ut på Hemnet. För att du skall kunna vara snabb när det väl gäller så är det viktigt att du har gjort en budget, ordnat med lånelöfte och framförallt bestämt dig för ett maxpris.

pexels-photo-54204-large

Det är lätt att ryckas med i budgivningarna och därför är det väldigt viktigt att veta när du skall tacka nej och hoppas på att det snart kommer ut en annan lägenhet som du är intresserad av. Det är svårare än man tror att dra sig ur en budgivning för en lägenhet man gärna vill ha. Om det blir du som tar hem budgivningen, se då till att skriva avtalet snabbt. För innan avtalet är underskrivet kan säljaren ångra sig.

Stort lycka till med din första lägenhetsaffär och dela gärna inlägget om du tror att andra kan ha nytta av de här tipsen!