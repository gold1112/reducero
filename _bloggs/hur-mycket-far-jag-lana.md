---
layout: posts
title: Hur mycket får jag låna? | Reducero
description: Reducero förklarar hur mycket du får låna och till vad.
keywords: hur mycket får jag låna
anchor: Hur mycket får jag låna?
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/hur-mycket-far-jag-lana

sitemap:
   priority: 0.8
   
tags:
  - lån
  - låna pengar

headline: Hur mycket får jag låna?
subline: ... och till vad?
background: /img/2016-11-23-hur-mycket-far-jag-lana.jpg
---

Hur mycket du får låna beror på din ekonomiska situation och vilken typ av lån du söker. Desto bättre ekonomiska förutsättningar du har desto högre belopp beviljar långivarna dig att låna men det maximala lånebeloppet varierar beroende på syfte och typ av lån, nedan reder vi ut en del av detta.

Om du [lånar pengar](/lana-pengar) med säkerhet tillåts du oftast att låna ett högre belopp än ett lån utan säkerhet då banken har en säkerhet i objektet du pantsätter.

#### Bostadslån

Med ett bostadslån kan du låna upp till 85% av bostadenspris, dvs. en bostad som kostar 1 miljon svenska kronor kan du belåna upp till 850,000 kronor. Det är dock inte alltid bara bostaden är den avgörande faktorn, din inkomst spelar också in och det är idag vanligt att långivarna inte låter dig låna mer än 5-6 gånger din årsinkomst.

#### Billån

Med ett billån kan du låna upp till 80% av bilens pris, en bil som kostar 100,000 kronor kan du alltså låna 80,000 kronor för att köpa, resterande 20,000 kronor behöver du själv betala, denna del kallas för kontantinsats. Om du saknar pengar till kontantinsatsen kan du låna till kontantinsatsen med ett lån utan säkerhet eller låna hela beloppet utan säkerhet, en form av [billån utan kontantinsats](/lan/billan-utan-kontantinsats) som är ett vanligt finansierings alternativ vid köp av begagnade bilar.

### Lån utan säkerhet

När du lånar utan säkerhet kan du använda pengarna till precis det du önskar och banken begär inte att du pantsätter något.

Genom Reducero kan du ansöka om att låna upp till 500 000 kronor utan säkerhet.

Samtliga långivare som är anslutna till Reducero prövar din ansökan för att ta reda på din ekonomiska situation och för att bedöma din framtida förmåga att betala tillbaka lånet.

Utifrån detta bestämmer sedan respektive bank och kreditgivare hur mycket pengar du får låna.

<div class="alert alert-warning" role="alert">
<p class="m-a-0"><strong>Tips.</strong> En bra riktlinje är att inte ansöka om ett lån som överstiger mer än 90% av din årsinkomst.</p>
</div>
