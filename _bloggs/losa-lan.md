---
layout: posts
title: Så enkelt löser du dyra lån - Reducero
description: Så enkelt löser du dyra lån, det kan spara dig tusentals kronor.
keywords: lösa lån, lös lån, hjälp med lån, hur löser jag lån
anchor: Så enkelt löser du dyra lån
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/losa-lan

sitemap:
   priority: 0.8
   
tags:
  - lösa lån
  - samla lån
  - samlingslån

headline: Så enkelt löser du dyra lån
subline: ...och sparar tusentals kronor
background: /img/2016-07-25-losa-lan.jpg
---

Kanske har du sett [lyxfällan](http://www.tv3play.se/program/lyxfallan) på tv och undrat över vad lösa lån betyder? Har du flera olika dyra krediter och smålån kan det vara värt att lära dig mer om att lösa lån, du kan spara tusenlappar varje månad och se till att komma på fötter igen. Nedan har vi samlat lite information om vad det innebär och hur du gör för att få ett samlingslån. 

### När ska man lösa lån?
Har du samlat på dig smålån, avbetalningslån, smslån och andra krediter? Då kan det vara läge att lösa dina lån och istället ta ett [samlingslån](/lan/samlingslan). Genom att ta ett större lån, hos en bank, kan du lösa dina dyra skulder och få ett lån med mer human ränta. Dessutom slipper du att hålla reda på alla räkningar som kommer i brevlådan. Varje räkning har en aviavgift som kan vara relativt dyr i längden. När du löser dina lån slipper du att betala flera aviavgifter och endast få en räkning varje månad. 

### Få ordning på din ekonomi
Alla människor kan hamna i ekonomisk knipa och det behöver inte vara ditt fel. Många kreditgivare tar ut ockerräntor och dyra avgifter varje månad, vilket kan få vem som helst att hamna i obalans. Det kan kännas som att det inte finns någon lösning på dina ekonomiska problem, men det gör det. Börja med att sluta ta fler dyra smålån och upphör med att konsumera onödiga saker. Nästa steg är att reda ut din ekonomiska situation; vad har du för skulder och vad har du för inkomster. Utan att ha fullständig koll på dina skulder kan det vara mycket svårt att bli ekonomiskt stark igen. Öppna alla brev, skriv ner vilka lån du har, ring dem om du behöver. Ta reda på vilka räntor, aviavgifter och övriga avigfter som finns på lånet. När du har en bra bild av din finansiella situation, är det dags att [ansöka om att lösa lån](/lan/losa-lan). 

<div class="img-x-100p">

[![lösa lån](/img/2016-07-25-losa-lan-banner.png)](/lan/losa-lan)

</div>

### Hur löser man lån?
När det blivit dags att skapa ordning i din ekonomi, och betala av alla onödiga lån och kreditkortsskulder, kan du ta hjälp av ett större samlingslån. Du får lånet utbetalt av banken och betalar själv av skulderna. Banken tvingar dig inte till att lösa dina skulder med det nya lånet du tagit, därför är det viktigt att du har en ekonomisk disciplin och verkligen gör det. Risken är annars stor att du sätter dig själv i en ännu större ekonomisk knipa. Ett samlingslån kan verkligen förbättra din ekonomi avsevärt och du kan njuta av hur skönt det är att få färre räkningar varje månad. De pengar du sparar kan du använda till något betydligt roligare än att betala av på lån.

### Vad krävs för att få ett samlingslån?
Det finns några grundkrav som du måste uppfylla; din inkomst bör vara tillräcklig för att kunna betala på lånet, du bör vara över 18 eller 20 år (olika krav för olika långivare), folkbokförd och skriven i Sverige, vara [skuldfri hos Kronofogden](https://www.kronofogden.se/). Helst ska du inte ha några betalningsanmärkningar, och om du har det sedan tidigare måste du visa på att du förbättrat din ekonomi och varit skötsam efter det. Hur mycket du får låna beror på storleken på din inkomst och hur mycket skulder du har. 
