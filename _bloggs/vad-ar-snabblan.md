---
layout: posts
title: Vad är ett snabblån? - Reducero
description: Reducero förklarar vad ett snabblån är och vad du bör tänka på innan du lånar.
keywords: snabblån, sms lån
anchor: Vad är ett snabblån?
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/vad-ar-snabblan

sitemap:
   priority: 0.8
   
tags:
  - lån
  - låna pengar
  - snabblån
  - smslån

headline: Vad är ett snabblån?
subline: ... och vad bör jag tänka på innan jag lånar
background: /img/2017-02-14-vad-ar-snabblan.jpg
---

Vi hör ofta om begreppet snabblån eller sms lån, inte minst i medierna, men vad menar vi egentligen när vi säger snabblån? Faktum är att någon enhetlig definition som alla kommit överens om inte finns men gemensamt för dessa lån brukar vara att lånet betalas ut relativt snabbt och utan säkerhet.

Lån utan säkerhet är i sig inget nytt fenomen. Namnet snabblån började användas brett i Sverige 2006, i samband med att ny teknik gjorde det möjligt att ansöka om ett mindre lån direkt i mobilen eller på internet.

#### Uppskattad och utskälld låneform

Många kunder uppskattade snabblånen, då man upplevde att låneformen fyllde en lucka i de etablerade aktörernas utbud genom att erbjuda ett enkelt sätt att snabbt ansöka om ett mindre belopp. 

Tidigare hade det ofta tagit flera dagar att ansöka om ett litet lån hos någon av de stora långivarna. Perfekt för den som fått en oväntad utgift som snabbt behövde lösas, tyckte många kunder. 

Men snabblånen fick också oerhört mycket kritik, ofta på grund av vad man menade var orimligt höga räntor. Oseriösa aktörer flockades dessutom till branschen och under några år var det något av en vilda västern på marknaden.

#### Snabblånemarknaden regleras

[Finansinspektionen (FI)](http://www.fi.se/) hängde inte med i svängarna men när myndigheten väl vaknade så reglerades marknaden successivt och flera oseriösa aktörer slogs ut. Sedan 2014 krävs ett särskilt tillstånd från FI för att bedriva kreditgivning mot konsumenter. 

Bland de första bolagen att få tillstånd från FI var [snabblåneföretaget Credigo](https://www.credigo.se/). Tillståndet innebär att verksamheten kontrolleras av FI, som löpande ska säkerställa att den uppfyller de krav som finns. Företag som inte sköter sig kan bli av med tillståndet och får därmed inte låna ut pengar. 

I takt med denna och andra regelskärpningar har marknaden för snabblån mognat och antalet aktörer minskat.

#### Vad har snabblånen gemensamt?

Förutom att lånet ofta betalas ut direkt efter godkänd kreditprövning är det dessutom vanligt med en kort återbetalningstid, inte sällan är den mellan 30 och 90 dagar. Det är också vanligt att det går att ansöka om ett mindre belopp utan UC, vilket inte är samma sak som att det går att låna utan att det tas en kreditupplysning. Alla företag är enligt lag skyldiga att göra en ordentlig kreditprövning.

Flera snabblåneföretag har istället valt att jobba med till exempel Creditsafe, som tar en kreditupplysning men utan att registrera den. Därför påverkas inte kreditvärdigheten negativt av en ansökan, till skillnad fån UC där varje ansökan registreras och mängden förfrågningar kan komma att påverka den framtida kreditvärdigheten.

#### Tänk efter innan du lånar

Som med alla lån ska du tänka efter noggrant innan du tar ett snabblån. En del aktörer lockar med räntefria lån första gången men fundera på om du verkligen behöver pengarna. Om du kommer fram till att du verkligen behöver ett litet lån, se till att jämföra villkoren hos de olika långivarna och glöm inte att hålla utkik efter dolda avgifter.
