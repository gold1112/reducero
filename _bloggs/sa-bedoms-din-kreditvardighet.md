---
layout: posts
title: Så bedöms din kreditvärdighet | Reducero
description: Vi går på djuper kring hur långivare, kreditgivare och kreditvärderingsinstitut bedömer din kreditvärdighet och lämnar även goda råd om hur du kan förbättra din kreditvärdighet.
keywords: kreditvärdighet, min kreditvärdighet
anchor: Så bedöms din kreditvärdighet
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/sa-bedoms-din-kreditvardighet

sitemap:
   priority: 0.8
   
tags:
  - lån
  - låna pengar

headline: Så bedöms din kreditvärdighet
subline: ... och så kan du förbättra den
background: /img/2016-10-19-sa-bedoms-din-kreditvardighet.jpg
---

Vi går på djuper kring hur långivare, kreditgivare och kreditvärderingsinstitut bedömer din kreditvärdighet och lämnar även goda råd om hur du kan förbättra din kreditvärdighet.

**Kreditvärdighet**

Kreditvärdighet är en persons eller ett företags förmåga att betala sina lån och andra skulder. Denna förmåga beror på ett stort antal faktorer men förenklas ofta i ratingklasser. Förmågan kan uttryckas från A till C, från 1 - 100 eller på många andra sätt där ena ändan av skalan indikerar mycket hög risk och den andra ändan av skalan indikerar låg risk. Bedömningarna görs ofta av kreditvärderingsinstitut.

En låg kreditrating innebär att det är hög risk att personen eller företaget inte kommer att kunna betala sina lån i tid. I huvudsak finns en skiljelinje mellan de som inte har några betalningsanmärkningar och som brukar få låna om inkomsten räcker för lånets storlek och de med betalningsanmärkningar som sällan beviljas lån och då till högre ränta.

Genom Reducero kan du [låna pengar](/lana-pengar) med och utan betalningsanmärkning. Du kan läsa mer om betalningsanmärkningar och om att låna med en betalningsanmärkning på andra sektioner på vår hemsida.

När långivare och andra kreditgivare ska behandla din låneansökan utgår de alltid från den kreditvärdighet du har. Din kreditvärdighet har således betydelse för dina möjligheter att få ett lån och bestämmer i hög utsträckning såväl lånebeloppets storlek som de villkor du erbjuds.

**Hur kan jag förbättra min kreditvärdighet?**

Din kreditvärdighet är en sammanvägning av en mängd faktorer som tillsammans visar din förmåga att betala lån och andra skulder.

Exakt hur kreditvärdigheten bestäms är affärshemligheter för kreditupplysningsföretagen men de faktorer som framförallt undersöks är:

* Inkomstuppgifter
* Antalet kreditengagemang och deras storlek
* Antalet registrerade kreditupplysningar
* Betalningsanmärkningar och andra anmärkningar
* Uppgifter om dina tillgångar så fordon och fastigheter
* Uppgifter som ditt civilstånd

Om du har betalningsanmärkningar påverkas din kreditvärdighet negativt av detta. En anmärkning försvinner automatiskt två till tre år efter dess tillkomst beroende på typ om du inte kan visa att anmärkningen är felaktig och får uppgiften rättad. På denna sida kan du läsa mer om hur du <a rel="nofollow" target="_blank" href="https://www.uc.se/kundservice/vanliga-fragor/kreditupplysningar.html">rättar en felaktig kreditupplysning</a>.

En annan sak du kan göra för att påverka din kreditvärdighet i positiv riktning är att ta bort eller betala av kreditlöften. Att du har en möjlighet att när som helst utnyttja en kredit innebär en osäkerhet som långivare måste kalkylera med. Om du tar bort osäkerhetsmomentet ökar också möjligheten att beviljas ett lån.

Utöver detta påverkas din kreditvärdighet positivt av att du ökar din inkomst och minskar dina utgifter.

Ytterligare saker som kan påverka din kreditvärdighet är; förändrat civilstånd eftersom det ofta medför förändringar i belastningen på ekonomin, förekomsten av äktenskapsförord, om du driver företag och i sådana fall i vilken verksamhetsform, ökat eller minskat taxeringsvärde för de fastigheter du äger, tidigare och nuvarande skuldsaldo, antalet kreditupplysningar under den senaste 12 månadersperioden och om du nyligen gått över någon kreditlimit.

Vissa företag erbjuder tjänster där man mer i detalj kan se en samlad bild av ens ekonomi. Du har också rätt att kostnadsfritt, en gång per år, vända dig till ett kreditupplysningsföretag och begära ut de uppgifter som finns registrerade om dig.
