---
layout: posts
title: Tjänsten som förändrat lånemarknaden - Reducero
description: bekvämt, enkelt, snabbt!
keywords: lån, jämföra lån, lånejämförelse, låna pengar, banken
anchor: Tjänsten som förändrat lånemarknaden
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/tjansten-som-forandrat-lanemarknaden

sitemap:
   priority: 0.8
   
tags:
  - lån
  - jämför lån
  - lånejämförelse
  - låna pengar
  - banken

headline: Tjänsten som förändrat lånemarknaden
subline: och ger dig som kund makten
background: /img/2016-08-14-tjansten-som-forandrat-lanemarknaden.jpg
---

Det är nya tider nu och det kräver moderna tjänster. Innan informationssamhället var konkurrensen mycket begränsad, vi valde produkter och tjänster utifrån vanor, rekommendationer och enligt vad våra närstående gjort i generationer. När stor del av handeln flyttade ut på Internet uppstod en ny möjlighet – att snabbt jämföra priset på varor och tjänster. Det tog lite längre tid innan jämförelsesiterna kom att inrikta sig på tjänster, då de är svårare att jämföra på grund av sina olikheter. Med avancerad teknologi kan vi numer även jämföra mer komplicerade tjänster så som lån.

### Betalar du för mycket för dina lån?

Ställ långivarna mot varandra och jämför deras bästa erbjudanden, det ger dig bäst förutsättningar att [låna pengar snabbt](/lana-pengar) med låg ränta och bra villkor. Att jämföra lån på nätet är bekvämt, enkelt och du får snabbt besked. Nu är det slut på att springa runt och jaga långivarna, eller sitta i långa telefonköer för att försöka få tag på din bank. Du behöver inte längre dansa efter bankens pipa och riskera att få ett nej rakt i ansiktet. Sluta betala för mycket för dina lån, [gör som en smart konsument – jämför lån](/lan/jamfor-lan)!

### Nu är det du som bestämmer

Reducero drivs av att stärka din position som låntagare i förhållande till långivaren. Vi har förstått att konkurrens är nyckeln till bra lånevillkor och därför utvecklat smarta och kraftfulla tjänster, som gör det enkelt att ställa långivare mot varandra och jämför deras bästa erbjudanden. Du behöver inte längre vänta i timmar på banken för att få prata med din personliga bankman. Du slipper att, öga mot öga, förklara din privata situation och be banken om att låna dig pengar. Nu ber långivarna istället DIG om att få låna ut pengar. Det är en revolution som du absolut ska utnyttja. Gå inte den bekväma vägen och ta första låneerbjudandet som dyker upp, det kommer din plånbok att tacka dig för.

### Kundtjänst i världsklass

Vår kundservice finns alltid där för dig om du behöver, alla vardagar via telefon och mail. När du ansöker om lån via Reducero ger vi dig alltid 30 dagars betänketid, så du har gott om tid att fundera innan du bestämmer dig. Du kan använda pengarna till vad du vill och har gott om tid att fundera i lugn och ro. Skulle du ångra dig kan du betala tillbaka hela lånet när du vill utan extra kostnader. 

### Tar Reducero endast en kreditupplysning?

Ja, vi gör endast en kreditupplysning som långivarna sedan får dela på. Du kan få flera omfrågandekopior från de långivare som har tittat på din kreditupplysning. Enligt lag får man alltid så kallade omfrågningskopior från de som tagit del av kreditupplysningen. Kopiorna är bara en information till dig som låntagare och påverkar inte din kreditvärdighet.

### När betalas lånet ut?

Beroende på vilken bank eller kreditgivare du väljer kan det ta olika lång tid för dig att erhålla dina pengar. När du beviljats och valt lån har du två alternativ att signera; aningen så skickas ett skuldebrev till dig med posten som du skriver under och returnerar, eller så öppnar du ett digitalt skuldebrev som du skriver under med BankID. När banken fått ett signerat skuldebrev via post tar det ca 2-5 arbetsdagar innan pengarna finns på ditt bankkonto. Du får vanligtvis pengarna utbetalda till ditt bankkonto redan samma dag om du signerar skuldebrevet med BankID. Du kan även gå direkt till bankens kontor (om din bank har ett fysiskt kontor) under kontorstid och skriver under skuldebrevet och få pengarna utbetalda eller överförda till ditt bankkonto. Ett annat alternativ är att få pengarna skickade till dig i form av en utbetalningsavi, som du sedan går med till en bank och de hjälper dig att lösa in den.

### Kan jag lösa lånet i förtid?

Du kan när som helst lösa lånet i förtid eller betala in extra pengar utan att det kostar någonting. Kontakta banken eller låneinstitutet så hjälper de dig.

### Hur kan Reducero vara gratis?

Att ansöka om lån med Reducero är helt gratis och det tillkommer inga räntepåslag eller dolda kostnader. Det är möjligt genom att Reducero tar ut en avgift av långivarna. Långivarna vill gärna synas hos Reducero eftersom det är en marknadskanal för dem, precis som TV- eller radioreklam. Därför har vi kunnat samla så pass många banker hos oss. Det är alltså bankerna som betalar för att du ska kunna jämföra lån. Gratis service - precis som det ska vara.

### Vilken ränta får jag?

Räntan sätts inte av Reducero utan av de långivare som prövar din ansökan. Varje bank och låneinstitut behandlar din ansökan individuellt. Den ränta som du erhåller bestäms utifrån den kreditupplysning som utförs av långivarna, samt deras interna riskbedömningar om din (och eventuell medsökandes) återbetalningsförmåga och kreditrisk. Räntan mellan olika banker och låneinstitut varierar beroende på vem eller vilka som söker, det är därför det är så viktigt att jämföra så många banker och låneinstitut som möjligt för att hitta den lägsta räntan för just dig. Bankerna ger dig inte en bättre ränta om du gör dig besväret att uppsöka ett bankkontor, bespara dig därför det extra arbetet och ansök online istället. Du måste skicka in en ansökan får att få reda på vilken ränta bankerna och låneinstituten kan erbjuda dig, det tar bara några minuter.

### Hur mycket får jag låna?

Med Reducero kan du låna från 5 000 kronor upp till 400 000 kronor. Samtliga långivare som är anslutna till Reducero prövar din ansökan för att ta reda på din ekonomiska situation och för att bedöma din framtida förmåga att betala tillbaka lånet. Utifrån detta bestämmer respektive bank och kreditgivare hur mycket du får låna. Vi rekommenderar dig inte att ansöka om ett lån som överstiger mer än 90% av din årsinkomst, då chansen är stor att få avslag. Tänk på att du när som helst kan lösa lånet i förtid eller betala in extra pengar utan att det kostar dig någonting.
