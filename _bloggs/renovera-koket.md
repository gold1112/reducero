---
layout: posts
title: Renovera inte köket förrän du läst det här! - Reducero
description: Tips inför köksrenoveringen - sakerna du måste ha koll på.
keywords: renovera köket, renovering, kökslån
anchor: Renovera inte köket förrän du läst det här!
author_member: jhon-smith
duration: 4 min read
permalink: /blogg/renovera-koket

sitemap:
   priority: 0.8
   
tags:
  - bostad
  - kök
  - lån till köket
  - kökslån
  - köksrenovering
  - renoveringslån

headline: Renovera inte köket förrän du läst det här!
subline: Råden som gör renoveringen till en dröm
background: /img/2016-06-07-renovera-koket.jpg
---

### Dags för ett nytt kök?

Är det dags att låta köket få en ansiktslyftning eller rent av ett helt nytt utseende? Även om en köksrenovering kan låta som en enkel uppfräschning av ditt hem så är det ett projekt som kräver en investering i tid och pengar. Det finns mycket att tänka på innan du drar igång en köksrenovering. Vare sig du har en stor budget eller om du tänker utföra en billigare renovering, finns det en hel del att tänka på innan du börjar. Nedan följer tips och råd till dig som drömmer om ett nytt kök.

### Vad finns det för fördelar med att renovera ditt kök?

På mäklarspråk brukar man prata om standardhöjande åtgärder, det är sånt som höjer värdet på din bostad. Ett trevligt kök är ett av de mest säljande argumenten i en bostad. ”Självklart drar ett fräscht kök upp priset. Till exempel kan ett trångt kök göras större genom att slå ut en vägg och få öppen planlösning.” Säger mäklare Jeanette Fahlström [(Expressen Leva & Bo 2015-08-10)](http://www.expressen.se/leva-och-bo/sa-hojer-du-vardet-pa-ditt-hem--enkla-knep/) 

Även om du har planer på att sälja din bostad kan ett kök vara en bra investering för dig personligen. Ofta tillbringar vi mycket av vår tid i köket. En trevlig och mysig köksmiljö påverkar vårt humör positivt och vardagsmatlagningen blir roligare. 

### Vad kostar det att renovera ett kök?

Att renovera köket kan kosta från några tusen till flera hundra tusen. Planering är A och O. Hur ser köket ut idag? Hur vill du att köket ska se ut? Behöver du förändra planlösningen, vattendragningar, el och avlopp etc? Ju mindre av detta som behöver ändras desto billigare blir renoveringen. 

Börja med att bestäm dig för vad det får kosta. När du har en maximal totalsumma klar för dig är det dags att göra en budget. I en budget bestämmer du hur stor del av medlen som ska gå till material, arbete och vitvaror. Lämna utrymme för oväntade kostnader som kan uppstå.

Helrenovering av ett kök betyder att allt från golv till tak byts ut, inklusive köksstommarna. En budgetvariant är att bara byta ut dina köksluckor. Det finns firmor som kommer hem och mäter och sedan levererar nya luckor till dig. Om du kompletterar nya luckor med att måla/tapetsera och lägga in ett nytt golv så har du helt plötsligt ett kök som ser nytt ut. 

[<img src="/img/2016-06-07-renovera-koket-banner.png" width="100%" alt="renoveringslån, låna till renovering">](/lan/renoveringslan)

### Vad ska jag tänka på när jag renoverar köket?

Det är en positiv förändring att ha ett nytt kök men tänk på att du får leva med resultatet länge och därför är det viktigt att allt blir rätt. Planera noga men ge utrymme för förändring under arbetets gång. 

Tänk igenom planlösningen, det ska t.ex. vara lätt att öppna kyl och frys. Arbetsflödet i köket måste funka, med dåliga avlastningsytor blir det jobbigt att laga mat. Återförsäljare av kök brukar vara mycket hjälpsamma med tips och råd. Det finns även tjänster online för att [planera ditt kök i spännande 3D-modeller.](http://www.ikea.com/ms/sv_SE/rooms_ideas/splashplanners_ihp.html)

Ett färgglatt kök kan kännas som en bra idé initialt, men hur många är det som är nöjda med sitt terrakottafärgade 90-talskök idag? Välj istället neutrala färger, som ser stilrena och snygga ut. Om du är sugen på färg kan du istället ta in färgranna accenter i köket, exempelvis färgpigga handdukar och grytlappar eller roliga köksapparater.

Var noga när du väljer material, speciellt när i valet av bänkskiva. Trä kan lätt få vatten och flottmärken samt behöver oljas in med jämna mellanrum. Rostfritt kan se flammigt ut och bli repigt. Stenskivor kan få märken och bli kantstötta. Laminatskivor är billiga men hållbarheten är oftast kortare än övriga material. 

När du väljer vitvaror är det viktigt att tänka på funktioner och vad de kostar i drift. Lista vad som är viktigast för dig; är det priset, att de är tystgående, är elsnåla, rymliga eller designen är prioriterad?

Glöm inte att fundera över var det ska vara ljus. Det är ett bra beslut att tidigt i processen att tänka på var det behövs belysning och var eluttagen ska sitta. När elektrikern väl är på plats kan det vara stressigt att fatta ett bra beslut. 

### Renovera köket själv eller anlita hantverkare

#### Om du gör jobbet själv

Många människor tycker att det kan vara avkopplande och spännande att renovera själv, men det kräver både nyfikenhet och en stor portion tålamod. Gör det du tycker är roligt och har kunskap att göra men var noga med att anlita fackfolk där det behövs. Tänk på att flera delar av köket måste vara fackmannamässigt utfört både för din egen säkerhet och för att försäkringar ska gälla. Är du det minsta tveksam, anlita hantverkare till att åtminstone göra el och VVS (vatten och avlopp) installationer. Du kan i värsta fall bli skadeståndsskyldig om arbetet blir fel utfört. Eftersom ROT-avdraget gäller för köksrenoveringar är det ett naturligt beslut att elektriker och rörmokare även anlitas så att installationen av vitvaror och belysning blir rätt utförda. 

Börja med att riva ut det gamla köket, det är kanske roligt till en början men kan ta lång tid och tråkiga överraskningar kan uppstå. Förbered för installationer. Börja renoveringen med att måla eller tapetsera och lägg in nytt golv. Du har förmodligen lagt en stor summa pengar på inredningen i köket, därför är det viktigt att du är noggrann så att det verkligen blir bra. Ett tips är att rita upp det nya köket på de väggar där de skall monteras. Det underlättar installation.

#### Om du anlitar hantverkare

El och VVS rekommenderas alltid att de görs av fackfolk. För dig som har ont om tid eller tummen mitt i handen är det vettigt att även ta hjälp av proffs för monteringen av inredningen. Tänk på att det kan skilja mycket i kvalitet, pris och tidsåtgång för olika hantverkare. Det bästa är om du har en referens från någon du känner som har gjort en köksrenovering och varit nöjd. Om du inte har det, var noga med att be om referenser och ring upp tidigare kunder. Det sista du vill är att sitta med ett halvfärdigt kök på grund av en oseriös hantverkare som aldrig dyker upp igen. 

Var noga med att presentera så mycket information som möjligt för hantverkaren så att kostnadsförslaget de ger är så nära sanningen som möjligt. Kolla runt med några olika firmor om möjligt. Det är ofta gratis att fråga olika hantverkarfirmor om kostnadsförslag. Se till att få en uppskattad tidsplan på papper, och var noga med att bestäm vad som händer om inte tidsplanen följs. Fråga även vad som händer om hantverkaren blir sjukskriven, finns det någon ersättare?

#### Renovera köket med ROT-avdrag

När du anlitar en hantverkare kan du göra ett så kallat ROT-avdrag. Du kan utnyttja ROT-avdraget om du har betalat in skatt och på så sätt har något att kvitta renoveringskostnaden mot. Under 2016 är ROT-avdraget 30 procent av arbetskostnaden på upp till 50 000 kr per person. För att få maximalt rotavdrag ska du därmed ha en arbetskostnad på 167 000 kr. Det är inte du själv som privatperson som söker om ROT-avdrag, det är den hantverkare du anlitar. Du får en förskottsrabatt på fakturan på jobbet och pengarna från Skatteverket får hantverkaren sedan.

Det finns en hel del regler kring ROT-avdraget som du kan läsa om på [Skatteverkets hemsida.](https://www.skatteverket.se/privat/fastigheterbostad/rotrutarbete.4.2e56d4ba1202f95012080002966.html) Till exempel måste den hantverkare du anlitar inneha F-skatt och får inte var en närstående till dig. Du måste äga bostaden och bo i den för att få rätt till avdrag. 

### Tidsplan

Var realistisk i din tidsplan, att renovera tar tid och kräver mycket tålamod. Lura inte dig själv med att tro att allt kommer blir klart på en vecka. Det minsta du kan räkna med är 14 dagar, från det att du börjar riva tills allt är färdigt, men troligtvis kommer det att ta mer än en månad även om du gör mycket själv på din fritid.

Det kan vara smart att renovera köket på sommaren eftersom det är lättare att klara sig utan köket när det är fint väder och man kan vara ute. 	

### En snabb checklista – 7 saker att tänka på
Drömmen om ett kök är närmare än du tror. Använd den här snabba checklistan för att starta igång ditt projekt smidigast och snabbast. 

1. Skriv en önskelista. Sätt dig ner, om ni är en familj sätt er ner tillsammans. Dröm fritt – i det här skedet är allt möjligt! Fundera över vad det är du verkligen vill. Vad ska köket användas till? Ska du bara laga mat eller ska det till exempel användas till att pyssla och läsa läxor? Om du har småbarn bör säkerheten komma i första hand. Säkra vitvaror som inte tippar och förvaring är viktiga aspekter när barn finns med i bilden. Ett bra kök ska även vara lättstädat. Fantisera om stil, ska ni ha det klassiskt eller mer trendigt? 

2. Samla information. Ta reda på allt du behöver veta om ditt kök. Mät upp och rita en skiss. Ta foton från lite olika vinklar. Prova att använda 3D-modeller online för att se vilka möjligheter till förändring som finns. Kolla upp dina ekonomiska förutsättningar, om du behöver låna till renovering kan ett [renoveringslån](/lan/renoveringslan) vara lösningen, ta reda på hur mycket du får låna. Undersök om det behövs bygglov. Skulle renoveringen av köket innebära ingrepp i hustes bärande konstruktioner, i ventilationen eller vatten- och avloppssystem ska du ha bygglov från kommunen och/eller tillstånd från föreningen om du bor i en bostadsrätt. Fråga efter planritningar och monteringsanvisningar från kökstillverkaren. En viktig sak att fundera över är ljus och ljuskällor.  

3. Inhämta inspiration. Läs inredningsmagasin och försök skissa lite själv. Inredningsarkitekter använder sig av något som heter ”mood boards”, där de samlar tapet- och färgprover, materialbitar med mera från vad de gillar och sätter upp på en tavla. Mood boards kan även göras virituellt, i valfritt bildbehandlingsprogram. Ett tips är att söka olika stilar på Instagram och Pinterest. Du kan även gå på köksutställningar och skaffa/beställa kataloger. Fråga dina vänner som gjort köksrenoveringar om vad de är nöjda med och vad de skulle vilja förbättra/ångrar. Ställ frågor på forum och ta hjälp av vänner.

4. Ta fram olika förslag. Ta fram olika ett underlag på egen hand eller ge en köksförsäljare förtroendet att arbeta igenom ett förslag. Många kökstillverkare (även IKEA) har personal eller arkitekter som ritar förslag på köksinredningar åt sina kunder. Det är oftast gratis och det finns inget krav på att du beställer just från dem i slutändan. Ge dig inte in på dyra speciallösningar om du inte har råd med det, men kompromissa inte med nödvändiga detaljer du inte kan vara utan. 

5. Jämför olika alternativ. När du har olika design och materialval att välja på gäller det att ta ställning till hur de förhåller sig i relation till budgeten. Glöm inte att du måste fundera över allt från knoppar och handtag till blandare. Små detaljer göra ett stort intryck och även vara kostsamma. 

6. Fatta ett beslut. Med ett bra beslutsunderlag är det nu dags att bestämma sig.

7. Håll budget och tidsplan. Samla ihop dina inköp, då får du bästa pris och koll på de totala kostnaderna. Förhandla alltid om rabatter, pruta! Vissa saker blir billigare att inhandla själv andra delar är billigare om hantverkaren köper. Spara alla kvitton och följ upp kontinuerligt under hela processen. 

Känner du någon som ska renovera köket? Dela gärna den här artikeln med någon genom att tagga dem. 
