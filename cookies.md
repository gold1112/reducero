---
layout: cookies
title: Cookies - Reducero
description: Här kan du läsa mer om hur Reducero använder Cookies, ett nödvändigt ting för att du ska kunna använda Reducero.
keywords: cookies reducero, cookies, cookieinformation

permalink: /cookies/

sitemap:
   priority: 0.9
   
---

# Om cookies

Cookies &auml;r n&ouml;dv&auml;ndiga f&ouml;r att du ska kunna anv&auml;nda Reducero och dess funktioner. En cookie &auml;r en liten textfil som lagras p&aring; anv&auml;ndarens dator och kan anv&auml;ndas till att rent tekniskt uppr&auml;tth&aring;lla en l&auml;nk mellan anv&auml;ndarens webbl&auml;sare och servern eller att lagra information som n&auml;r en anv&auml;ndare senast bes&ouml;kte webbsidan.

Cookien inneh&aring;ller ingen personlig information eller virus och den kan inte heller f&ouml;rst&ouml;ra annan information p&aring; din dator.

## Olika typer av kakor

Det finns tv&aring; typer av cookies; tillf&auml;lliga och s&aring;dana som lagras p&aring; anv&auml;ndarens dator. De tillf&auml;lliga &auml;r av den typ som under tiden en webbl&auml;sare anv&auml;nds kommer ih&aring;g information som har sparats i en s.k sessions-cookie. Denna typ av cookie tas bort n&auml;r webbl&auml;saren st&auml;ngs ner.

Den andra typen av cookies lagras p&aring; bes&ouml;karens dator under s&aring; l&aring;ng tid som webbsidan st&auml;llt in att cookien ska sparas.

## Om du inte gillar kakor

Om du vill kan du st&auml;nga av cookie-funktionen i din webbl&auml;sare. Det g&ouml;r du genom att g&aring; in i s&auml;kerhetsinst&auml;llningarna i din webbl&auml;sare. Det kan dock inneb&auml;ra att du inte kan anv&auml;nda Reducero nu eller i framtiden.

## S&aring; anv&auml;nder vi kakor

Reducero anv&auml;nder cookies f&ouml;r att h&aring;lla reda p&aring; n&auml;r du som anv&auml;ndare loggat in f&ouml;r att ta del av dina l&aring;neerbjudanden. Andra anv&auml;ndningsomr&aring;den: F&ouml;r att h&aring;lla reda p&aring; ifr&aring;n vilken k&auml;lla bes&ouml;kare kommer in p&aring; Reducero, via t.ex. Google eller en reklamannons.

All cookieinformation som anv&auml;nds p&aring; Reducero sparas helt anonymt.

Reducero anv&auml;nder f&ouml;ljande cookies genom f&ouml;ljande tj&auml;nster

Google Tag Manager

Hanterar JavaScript och HTML-taggar som anv&auml;nds f&ouml;r sp&aring;rning och analys

Google Optimize

Google Optimize &auml;r till f&ouml;r AB testning, testning & personaliserings verktyg f&ouml;r sm&aring; & stora f&ouml;retag.

Google Analytics

Web analytics tj&auml;nst som sp&aring;rar och rapporterar webbplats trafik

Facebook

M&auml;ta interaktion efter annonsering p&aring; Facebook och f&ouml;rb&auml;ttrad marknadsf&ouml;ring

Bing

M&auml;ta interaktion efter annonsering p&aring; Bing och f&ouml;rb&auml;ttrad marknadsf&ouml;ring

Inspectlet

Web anblytics tj&auml;nst f&ouml;r att sp&aring;ra och f&ouml;lja beteende p&aring; webbplatsen