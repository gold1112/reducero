---
layout: cookies
title: Klagomål - Reducero
description: Här kan du läsa mer om hur Reducero bevarar dina personuppgifter, ett nödvändigt ting för att du ska kunna använda Reducero.
keywords: cookies reducero, cookies, cookieinformation

sitemap:
   priority: 0.9
   
permalink: /klagomal/
---

# Klagomål

Reducero värdesätter alla synpunkter och klagomål på vår verksamhet. Det ger oss möjlighet att fånga upp eventuella problem, åtgärda dessa och vidta förebyggande åtgärder.

## Klagomålshantering

Om du som kund har synpunkter eller klagomål, ber vi dig därför i första hand att framföra detta till vår kundservice som kan försöka lösa ditt problem på bästa sätt. Om den enskilda handläggaren inte kan hjälpa dig på egen hand tar hon eller han hjälp av sina kollegor och/eller avdelningsansvarig.

Om du fortfarande är missnöjd med vår hantering av en finansiell tjänst eller produkt kan du skriftligen framföra ditt klagomål via e-post eller brev till klagomålsansvarig på Reducero. Klagomålsansvarig har som uppgift att besvara klagomål och ser till att dina intressen som kund blir tillgodosedda.

## Kontaktuppgifter

Kontakta klagomålsansvarig genom att maila till klagomal@reducero.se, du kan också skicka ditt klagomål till vår postadress.

Postadress:
Reducero AB
Falkenbergsgatan 3, 412 85 Göteborg

Om det fortfarande finns meningsskiljaktigheter, har du möjlighet att få frågan prövad hos allmänna reklamationsnämnden eller allmän domstol.

Postadress:
Allmänna Reklamationsnämnden
Box 174, 101 23 Stockholm
Hemsida: www.arn.se

Du kan även erhålla vägledning om bank och försäkring från Konsumenternas Bank- och finansbyrå och Konsumenternas försäkringsbyrå.

Postadress:
Konsumenternas Bank- och finansbyrå
Box 24215, 104 51 Stockholm
Telefon: 0200-22 58 00
Hemsida: www.konsumenternas.se