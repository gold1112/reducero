---
layout: blogg
permalink: /blogg/
sitemap:
   priority: 0.9
---
<main class="blog-listing">
	<div class="blog-listing__container">
		<ul class="blog-listing__list">
			{% for post in site.bloggs %}
				<li>
					<h2 class="blog-listing__title"><a href="{{ site.baseurl }}{{ post.url }}">{{ post.anchor }}</a></h2>
					
					{% if post.author_member %}
						{% assign author_id = post.author_member | prepend: "/authors/" | append: ".html" %}
						{% assign author = site.authors | where: "url", author_id | first %}
						<div class="blog-listing__author-info">
								<div class="blog-listing__img"><img src="{{ site.baseurl }}{{ author.image }}" alt=""></div>
								<div class="blog-listing__author">
										<div>
												<p class="blog-listing__name"><span>by </span>
														<a class="blog-listing__name-link" href="">{{ author.name }}</a>
												</p>
												<p class="blog-listing__category"><span>in </span>
														<a class="blog-listing__category-link" href="">{{ author.role }}</a>
												</p>
										</div>
										<div class="blog-listing__duration">{{ post.duration }}</div>
								</div>
						</div>
					{% endif %}

					<div class="blog-listing__thumbnail"><a href="{{ site.baseurl }}{{ post.url }}"><img src="{{ site.baseurl }}{{ post.background }}" alt=""></a></div>
					<div class="blog-listing__content">
							{{ post.content | strip_html | truncatewords: 50 }}
							<a class="blog-listing__more" href="{{ site.baseurl }}{{ post.url }}">Read more</a>
					</div>
					<div class="blog-listing__tags">
							<span>Tags:</span>
							<ul class="blog-listing__tags-list">
									{% for category in post.tags %}
											<li>{{ category | capitalize }}</li>
									{% endfor %}
							</ul>
					</div>
				</li>
			{% endfor %}
		</ul>
	</div>
	{% include content-cta.html %}
</main>

